# Temat projektu: Aktywny firewall
Artur Muroń

## Ubuntu

## Instrukcje kompilacji, instalacji i konfiguracji

Instalacja: ubuntu 20.04 LTS na wirtualniej maszynie(np.VMWare)

Potrzebna jest instalacja pythona3 i snorta:

### python
```console
apt install python3
```

### snort 
```console
apt-get update
apt-get install licpcap-dev bison flex
apt-get install snort
```

### konfiguracja
```console
rm /etc/snort/snort.conf
cp ./snort/snort.conf /etc/snort/snort.conf

rm /etc/snort/rules/local.rules
cp ./snort/local.rules /etc/snort/rules/local.rules
```

### uruchomienie
```console
sudo snort -d -l /var/log/snort/ -A console -c /etc/snort/snort.conf | sudo ./firewall_ubuntu.py
```

# Opis projektu
W ramach tego zadania należało zbudować system zawierający zestaw adaptatywnych skryptów których celem
jest ochrona wewnętrznych zasobów przez automatycznymi atakami z zewnątrz.

Do ochrony systemu użyto linuxowego iptables, który konfigurowany na bieżąco przez pythonowy skrypt potrafi bronić się przed atakami.
Oprócz tego niezbędny jest system wykrywania zagrożeń - tutaj użyty snort.
Na podstawie wykrytych zagrożeń ustawiane są odpowiednie reguły.

# Szczegóły implementacyjne

Rozróżnianych jest 6 rodzajów ataków:

+"ICMP FLOOD"
+"ICMP FLOOD POSSIBLE"
+"TCP PORT SCAN"
+"UDP PORT SCAN"
+"LAND ATTACK"
+"UDP FLOOD"

Na podstawie wagi priorytetu ataku zapisanego w kodzie(attack_priorities) blokowane są odpowiednie adresy IP.
Przy odpowiednio dużej ilości ataków(lub gdy atak jest uznany za priorytetowy) ich waga zostaje przekroczona i dany adres zostaje zablokowany na minutę.

Priorytety ustawione są według możliwego zagrożenia. ICMP Flood jest najbardziej niebezpiecznym i taki adres IP zostaje od razu zablokowany.

## Testy
Aby przetestować ułożono specjalne reguły(local.rules).
Z innej instalacji wysyłany jest atak, który odbiera uruchumiony system (snort + firewall).
Host ustawiony na 10.0.2.15
### "ICMP FLOOD"

Zalanie  ofiary ogromną ilością paktietów ICMP.
atak:
```console
hping3 -1 -V -c 1000 --flood 10.0.2.15
```

reguła:
```console
alert icmp any any -> 192.168.100.1 any (msg:"ICMP FLOOD"; detection_filter:track by_dst, count 300, seconds 2; sid:1000003; rev:1;)
```


### "TCP PORT SCAN"
atak:
```console
nmap -sT 10.0.2.15
```

reguła:
```console
alert tcp any any -> 192.168.100.1 any (msg:"TCP PORT SCAN";flow:stateless; detection_filter:track by_src, count 200, seconds 1; sid:1000005; rev:1;)
```

### "UDP PORT SCAN"

atak:
```console
nmap -sU -v -p \ 10.0.2.15
```

reguła:
```console
alert udp any any -> 192.168.100.1 any (msg:"UDP PORT SCAN"; detection_filter:track by_src, count 15, seconds 3; sid:1000006; rev:1;)
```

### "LAND ATTACK"
Wysłanie na otwarty port hosta złośliwego pakietu. W rezultacie host odpowiada sam do siebie w nieskończoność
atak:
```console
hping3 -V -c 1 -d 100 -S -p 80 -s 80 -k -a 10.0.2.15 10.0.2.15
```

reguła:
```console
alert tcp any any -> 192.168.100.1 80 (msg:"LAND ATTACK"; sameip; flags:S; sid: 1000007; rev:1;)
```

### "UDP FLOOD"
Zalanie pakietami na otwarty port UDP.
atak:
```console
hping3 -2 -V -c 100 -d 100 -S -p 21 --flood 10.0.2.15
```

reguła:
```console
alert udp any any -> 192.168.100.1 21 (msg:"UDP FLOOD"; detection_filter:track by_dst, count 20, seconds 2; sid:1000008; rev:1;)
```

## Windows

## Instrukcje kompilacji, instalacji i konfiguracji

Instalacja: Windows 10

Potrzebna jest instalacja pythona3 i snorta:

### python
Należy pobrać pythona3:
[https://www.python.org/downloads/release/python-395/]

### snort 
należy użyć 
windows installation z https://www.snort.org/


Domyślnym adresem instalacji jest C:/Snort - jeśli został użyty inny folder należy zmienić poniższe instrukcje.
Następnie w folderze <zapisany-snort>/etc należy podmienić plik snort.conf z plikiem snort.conf z projektu.
Do folderu C:/Snort/rules należy skopiować plik local.rules z projektu.

# Opis projektu

Zasada użycia jest podobna jak na ubuntu. Do ochorny systemu wykorzystano Windows Defender Firewall z uzyciem netsh.
Oprócz tego konieczny jest program Snort do wykrywania zagrożeń. Z użyciem netsh z poziomu skryptu pythonowego ustawiane są odpowiednie reguły.

Rodzaje reguł, ataków i strategii odpowiedzi na nie są podobne jak na Ubuntu.



